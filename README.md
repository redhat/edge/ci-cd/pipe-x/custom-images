# Custom-Images

This repo contains custom images for testing use cases.

## Setup your build env

To prepare build env please follow upstream AutoSD docs:

- <https://sigs.centos.org/automotive/building/#installing-osbuild-on-centos-stream-9-fedora-or-rhel>
- <https://sigs.centos.org/automotive/building/#adding-some-dependancies>

```bash
cd sample-images/osbuild-manifests
git clone https://gitlab.com/redhat/edge/ci-cd/pipe-x/custom-images.git
```

*Note:* verify both image directories are on the same level

```
| \- images
| \- custom-images
```

## image manifests exist in this directory

- ps.mpp.yml

- qa.mpp.yml

Image used to run QA tests.

- qm.mpp.yml

Image used to run QA tests with qm and bluchi uinstalled in it

## Building an image

To include manifest from this repo set IMAGEDIR shell environment parameter
To set ssh options please set OS_OPTIONS array also

Sample for OS_OPTIONS
```bash
OS_OPTIONS+=("extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]")
OS_OPTIONS+=("ssh_permit_password_auth=false")
OS_OPTIONS+=("image_size=\"10000000000\"")
OS_OPTIONS+=("ssh_permit_root_login=true")

make IMAGEDIR="./custom-images"  DEFINES="${OS_OPTIONS[*]}" autosd-qemu-qm-regular.x86_64.qcow2
```
